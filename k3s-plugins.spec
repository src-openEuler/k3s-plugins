%global tagname k3s1

Name:    k3s-plugins
Version: 1.1.1
Release: 3
Summary: some standard networking plugins, maintained by the CNI team
URL    : https://github.com/rancher/plugins
Source0: https://github.com/rancher/plugins/archive/refs/tags/v%{version}-%{tagname}.tar.gz
#source1 version sys@v0.0.0-20220908164124-27713097b956
Source1: sys.tar.gz
License: Apache-2.0

BuildRequires: golang >= 1.8.3 libseccomp-devel btrfs-progs-devel procps-ng

%description
some standard networking plugins, maintained by the CNI team

%prep

%build
PKG="github.com/k3s-io/k3s"
PKG_K8S_CLIENT="k8s.io/client-go/pkg"
PKG_K8S_BASE="k8s.io/component-base"
PKG_CRICTL="github.com/kubernetes-sigs/cri-tools/pkg"
PKG_CONTAINERD="github.com/containerd/containerd"
PKG_CNI_PLUGINS="github.com/containernetworking/plugins"
GO=${GO-go}
TAGS="apparmor seccomp netcgo osusergo providerless urfave_cli_no_docs"
VERSION="v1.24.2+k3s-"
TREE_STATE="clean"
buildDate=$(date -u '+%Y-%m-%dT%H:%M:%SZ')
VERSION_CRICTL="v1.24.0-k3s1"
VERSION_CONTAINERD="v1.6.6-k3s1"
PKG_K3S_CONTAINERD="github.com/k3s-io/containerd"
VERSION_CNIPLUGINS="v1.1.1-k3s1"
VERSION_FLANNEL="v0.18.1"
VERSIONFLAGS="
    -X ${PKG}/pkg/version.Version=${VERSION}
    -X ${PKG}/pkg/version.GitCommit=${COMMIT:0:8}

    -X ${PKG_K8S_CLIENT}/version.gitVersion=${VERSION}
    -X ${PKG_K8S_CLIENT}/version.gitCommit=${COMMIT}
    -X ${PKG_K8S_CLIENT}/version.gitTreeState=${TREE_STATE}
    -X ${PKG_K8S_CLIENT}/version.buildDate=${buildDate}

    -X ${PKG_K8S_BASE}/version.gitVersion=${VERSION}
    -X ${PKG_K8S_BASE}/version.gitCommit=${COMMIT}
    -X ${PKG_K8S_BASE}/version.gitTreeState=${TREE_STATE}
    -X ${PKG_K8S_BASE}/version.buildDate=${buildDate}

    -X ${PKG_CRICTL}/version.Version=${VERSION_CRICTL}

    -X ${PKG_CONTAINERD}/version.Version=${VERSION_CONTAINERD}
    -X ${PKG_CONTAINERD}/version.Package=${PKG_K3S_CONTAINERD}

    -X ${PKG_CNI_PLUGINS}/pkg/utils/buildversion.BuildVersion=${VERSION_CNIPLUGINS}
    -X ${PKG_CNI_PLUGINS}/plugins/meta/flannel.Program=flannel
    -X ${PKG_CNI_PLUGINS}/plugins/meta/flannel.Version=${VERSION_FLANNEL}
    -X ${PKG_CNI_PLUGINS}/plugins/meta/flannel.Commit=${COMMIT}
    -X ${PKG_CNI_PLUGINS}/plugins/meta/flannel.buildDate=${buildDate}
"    
LDFLAGS=" -w -s"

mkdir -p .gopath/src/github.com/containernetworking/
cp %{SOURCE0} .gopath/src/github.com/containernetworking
pushd .gopath/src/github.com/containernetworking/
tar -xvf v1.1.1-k3s1.tar.gz
mv plugins-1.1.1-k3s1 plugins
%ifarch loongarch64
rm -rf plugins/vendor/golang.org/x/sys
tar -xf %{SOURCE1} -C plugins/vendor/golang.org/x/
%endif
rm -rf v1.1.1-k3s1.tar.gz
popd

OutputDir=`pwd`/.gopath/cni
GOPATH=`pwd`/.gopath
pushd .gopath/src/github.com/containernetworking/plugins
GO111MODULE=off GOPATH=$GOPATH CGO_ENABLED=0 "${GO}" build -tags "$TAGS" -buildmode=pie -ldflags "$VERSIONFLAGS $LDFLAGS  -linkmode=external -extldflags '-Wl,-z,relro  -Wl,-z,now  ' " -o $OutputDir
popd

%install
install -d $RPM_BUILD_ROOT/%{_datadir}/k3s
install -p -m 755 ./.gopath/cni $RPM_BUILD_ROOT/%{_datadir}/k3s/

%files
%{_datadir}/k3s/

%changelog
* Tue Jul 11 2023 huajingyun <huajingyun@loongson.cn> - 1.1.1-3
- Add loong64 support

* Thu Feb 9 2023 liyanan <liyanan32@h-partners.com> - 1.1.1-2
- Add PIE, BIND_NOW, RELRO  secure compilation options

* Mon Oct 10 2022 Ge Wang <wangge20@h-partners.com> - 1.1.1-1
- Init package
